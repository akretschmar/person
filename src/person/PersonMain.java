
package person;


public class PersonMain {
   public static void main(String[] args) {
       Person.Weight kg  = Person.Weight.KG; 
       Person.Weight lbs  = Person.Weight.LBS;
       
       Person.Height m = Person.Height.METERS;
       Person.Height in = Person.Height.INCHES;
       
       
       Person p1 = new Person("John", 82, 1.72); 
       Person p2 = new Person("Jane", 68.9,1.77);
       
       System.out.println(p1.getName() + " weighs " + p1.getWeight() + " " + kg + " and is " + p1.getHeight() + " " + m + " tall." );
       System.out.println(p2.getName() + " weighs " + p2.getWieghtLbs()+ " " + lbs + " and is " + p1.getHeightInches() + " " + in + " tall." );
    }
    
}
