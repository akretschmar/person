package person;



public class Person {
    private String name;
    private double weight;
    private double height;
    
    //enumerations
    public enum Weight{KG, LBS};
    public enum Height{METERS, INCHES};
    
    //Constructors
    public Person(){}
    
    public Person(String name, double weight, double height) {    
        this.name = name;
        this.weight = weight;
        this.height = height;
    }

    //Getters
    public String getName() {
        return name;
    }

    public double getWeight() {
        return weight;
    }

    public double getHeight() {
        return height;
    }
    
    public double getWieghtLbs(){
        return this.weight * 2.2;
    }
    
    public double getHeightInches(){
        return this.height * 39.37;
    }
    
    //Setters
    public void setName(String name) {
        this.name = name;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void setHeight(double height) {
        this.height = height;
    }
    
    public double getBMI(){
        return weight / (height * height);
    }

    
    
}
